-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2020 at 02:10 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automatic_question_paper_generator`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `subject_id` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `marks` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `name`, `marks`) VALUES
(1, 9, '1. Number Systems and Codes', 14),
(2, 9, '2. Boolean Algebra and Logic Gates', 20),
(3, 9, '3. Combinational Logic Design', 24),
(4, 9, '4. Sequential Logic Design', 18),
(5, 9, '5. Introduction to VHDL', 14),
(6, 9, '6. Digital Logic Families', 10),
(7, 10, '1. Electronic Circuits:Bipolar junction transistor', 14),
(8, 10, '2. Power Amplifiers', 20),
(9, 10, '3. Electronic Circuits : Operational Amplifier and its applications', 24),
(10, 10, '4. Communication Fundamentals: Analog Communication', 18),
(11, 10, '5. Pulse Modulation and Multiplexing', 14),
(12, 10, '6. Communication Fundamentals: Information theory', 10),
(13, 11, '1. Introduction to Data Structures', 14),
(14, 11, '2. Stack and Queues', 20),
(15, 11, '3. LinkedList', 24),
(16, 11, '4. Trees', 18),
(17, 11, '5. Graphs', 14),
(18, 11, '6. Sorting & Searching', 10),
(19, 17, '1. Introduction to analysis of algorithm', 14),
(20, 17, '2. Dynamic Programming Approach', 20),
(21, 17, '3. Greedy Method Approach', 24),
(22, 17, '4. Backtracking and Branch-and-bound', 18),
(23, 17, '5. String Matching Algorithms', 14),
(24, 17, '6. Non-deterministic polynomial algorithms', 10),
(25, 18, '1. Data Representation and Arithmetic Algorithms', 14),
(26, 18, '2. Processor Organization and Architecture', 20),
(27, 18, '3. Control Unit Design', 24),
(28, 18, '4. Memory Organization', 18),
(29, 18, '5. I/O Organization and Peripherals', 14),
(30, 18, '6. Advanced Processor Principles', 10),
(31, 19, '1. Introduction and Overview of Graphics System', 12),
(32, 19, '2. Output Primitives', 18),
(33, 19, '3. Two Dimensional Geometric Transformations', 20),
(34, 19, '4. Two Dimensional Viewing and Clipping', 18),
(35, 19, '5. Three Dimensional Object Representation', 12),
(36, 19, '6. Visible Surface Detection', 10),
(37, 19, '7. Illumination Models and Surface Rendering', 10),
(38, 16, '1. Operating System Overview', 14),
(39, 16, '2. Process Concept and Scheduling', 20),
(40, 16, '3. Synchronization and Deadlocks', 24),
(41, 16, '4. Memory Management', 18),
(42, 16, '5. File Management', 14),
(43, 16, '6. Input /Output Management', 10),
(44, 1, '1. Quantum Physics', 14),
(45, 1, '2. Crystallography', 20),
(46, 1, '3. Semiconductor Physics', 24),
(47, 1, '4. Interface in Thin Film', 18),
(48, 1, '5. Superconductors and Supercapacitors', 14),
(49, 1, '6. Engineering Materials and Applications ', 10),
(50, 2, '1. Atomic and Molecular Structure', 14),
(51, 2, '2. Aromatic systems &their molecular structure', 20),
(52, 2, '3. Intermolecular Forces & Critical Phenomena', 24),
(53, 2, '4. Phase Rule-Gibb’s Phase Rule', 18),
(54, 2, '5. Polymers', 14),
(55, 2, '6. Water', 10),
(56, 3, '1. System of Coplanar Forces', 14),
(57, 3, '2. Equilibrium of System of Coplanar Forces', 20),
(58, 3, '3. Friction', 24),
(59, 3, '4. Kinematics of Particle', 18),
(60, 3, '5. Kinematics of Rigid Body', 14),
(61, 3, '6. Kinetics of a Particle', 10),
(62, 4, '1. DC Circuits', 14),
(63, 4, '2. AC Circuits', 20),
(64, 4, '3. Generation of Three-Phase Voltages', 24),
(65, 4, '4. Transformers', 18),
(66, 4, '5. Electrical Machines', 14),
(67, 4, '6. Principle of operation of Single-Phase induction motors', 10),
(68, 5, '1. Diffraction', 14),
(69, 5, '2. Laser and Fibre Optics', 20),
(70, 5, '3. ElectroDynamics', 24),
(71, 5, '4. Relativity', 18),
(72, 5, '5. Nanotechnology', 14),
(73, 5, '6. Physics of Sensors', 10),
(74, 6, '1. Principles of Spectroscopy', 14),
(75, 6, '2. Applications of Spectroscopy', 20),
(76, 6, '3. Concept of Electrochemistry', 24),
(77, 6, '4. Corrosion', 18),
(78, 6, '5. Green Chemistry and Synthesis of drugs', 14),
(79, 6, '6. Fuels and Combustion', 10),
(80, 8, '1. Introduction to Engineering Graphics', 12),
(81, 8, '2. Projection of Points and Lines', 18),
(82, 8, '3. Projection of Solids', 20),
(83, 8, '4. Section of Solids', 18),
(84, 8, '5. Sectional Orthographic Projection', 12),
(85, 8, '6. Missing Views', 10),
(86, 8, '7. Isometric Views', 10),
(87, 7, '1. Fundamentals of C Programming', 14),
(88, 7, '2. Branching and looping structures', 20),
(89, 7, '3. Functions', 24),
(90, 7, '4. Arrays and Strings', 18),
(91, 7, '5. Structure and Union', 14),
(92, 7, '6. Pointers', 10);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marks` int(11) NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `chapter_id`, `name`, `marks`, `level`, `created_at`, `updated_at`) VALUES
(1, 38, 'Commodi consequatur consequatur beatae.?', 6, 'Difficult', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(2, 42, 'Dicta laborum impedit saepe soluta est et.?', 6, 'Difficult', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(3, 42, 'Voluptates officia eveniet aut debitis nostrum.?', 6, 'Easy', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(4, 39, 'Qui sed reiciendis beatae mollitia doloremque.?', 6, 'Easy', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(5, 42, 'Eum earum itaque est vel qui et ipsam aut rem quis et omnis.?', 2, 'Moderate', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(6, 43, 'Quidem magnam recusandae culpa aliquid dignissimos ipsam autem doloremque voluptatem.?', 4, 'Moderate', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(7, 38, 'Quo consectetur libero sunt a.?', 6, 'Easy', '2020-07-04 08:51:57', '2020-07-04 08:51:57'),
(8, 39, 'Velit aut in voluptates.?', 8, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(9, 39, 'Est vitae omnis rerum nihil corporis consequuntur officiis asperiores.?', 4, 'Difficult', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(10, 39, 'Odit eos dolor consequatur quis qui.?', 4, 'Easy', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(11, 43, 'Sed nihil dolores est odio temporibus quo similique voluptas amet sit repudiandae eveniet non ipsa.?', 8, 'Difficult', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(12, 39, 'Dolorum numquam dolor velit.?', 8, 'Difficult', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(13, 43, 'Fugiat aut fugiat et voluptates eius aperiam rerum dolor rem expedita veniam.?', 4, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(14, 39, 'Unde modi et rem molestiae aliquam aut dolor impedit maiores consequatur consequatur quas magnam.?', 2, 'Easy', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(15, 42, 'Quia vitae qui excepturi blanditiis tenetur sed labore vitae nihil eos et.?', 8, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(16, 42, 'Dolores tempore cum natus nihil pariatur repudiandae ut quis eveniet.?', 4, 'Easy', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(17, 39, 'Autem explicabo temporibus ut.?', 6, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(18, 42, 'Officiis et nulla molestiae sunt quas quis ut nostrum occaecati ea maiores.?', 2, 'Easy', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(19, 43, 'Voluptatum sint consequatur possimus quo nobis vitae consequatur est nulla qui.?', 8, 'Difficult', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(20, 38, 'Vero atque reprehenderit cum distinctio hic quis ducimus omnis doloribus nesciunt.?', 4, 'Difficult', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(21, 40, 'Facere sint numquam possimus non aut.?', 6, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(22, 40, 'Magni voluptatem magni ea cupiditate et soluta ut voluptate.?', 4, 'Moderate', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(23, 43, 'Quod mollitia non earum veniam alias minus dolorem.?', 6, 'Easy', '2020-07-04 08:51:58', '2020-07-04 08:51:58'),
(24, 41, 'Ipsam ea maiores vel a et.?', 6, 'Moderate', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(25, 40, 'Aliquid et vero repellendus.?', 2, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(26, 40, 'Quaerat maiores doloremque recusandae totam ab et rerum aperiam ut officia.?', 2, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(27, 39, 'Delectus et magni a sed.?', 4, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(28, 41, 'Voluptate deleniti quis assumenda dolore amet dolor iusto.?', 8, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(29, 39, 'Magni reiciendis nam quasi similique.?', 4, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(30, 42, 'Cumque voluptatem occaecati et porro laborum soluta doloremque aliquam non corporis.?', 8, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(31, 43, 'Rerum veritatis doloribus natus vel officia eveniet libero sint dolore.?', 6, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(32, 39, 'Vero atque quae vero consequatur voluptates et rerum sit.?', 2, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(33, 40, 'Perspiciatis quia dolorem distinctio fugiat.?', 6, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(34, 42, 'Fugit dicta velit voluptatem dolor aliquid et facere magnam sed.?', 4, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(35, 43, 'Est exercitationem ut quis mollitia eos laudantium beatae quia nobis aut veritatis quae.?', 8, 'Moderate', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(36, 39, 'Qui maxime nam a deleniti quaerat.?', 6, 'Moderate', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(37, 38, 'Qui omnis ipsa deleniti fugit.?', 6, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(38, 40, 'Sunt aperiam est pariatur soluta numquam fugit harum dolorum consequatur ut et.?', 4, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(39, 38, 'Alias enim nihil suscipit totam facere doloremque voluptatem.?', 2, 'Moderate', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(40, 42, 'Recusandae inventore unde aliquid unde et qui nostrum quos molestias suscipit et.?', 4, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(41, 43, 'Harum et dolores minus non est accusantium minima voluptatem placeat qui nam est.?', 8, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(42, 39, 'Dolor aut tempora provident sed repellat accusamus rerum quis necessitatibus debitis.?', 6, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(43, 39, 'Eos officiis repellat explicabo officiis ex laudantium officia dolorum.?', 4, 'Easy', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(44, 41, 'Maiores laudantium pariatur et ab et dolores recusandae ullam incidunt nobis veritatis ducimus.?', 2, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(45, 40, 'Odit repellat eos repellat ea ducimus similique aliquam est libero corrupti reiciendis consequuntur.?', 2, 'Difficult', '2020-07-04 08:51:59', '2020-07-04 08:51:59'),
(46, 40, 'Reiciendis et ipsam ipsum.?', 4, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(47, 42, 'Eos voluptate est deleniti quibusdam.?', 4, 'Difficult', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(48, 41, 'Id ea doloremque.?', 8, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(49, 38, 'Natus error vitae adipisci.?', 8, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(50, 39, 'Sequi consequatur nemo dolorum minus et ut molestiae at rerum nesciunt possimus ipsa.?', 6, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(51, 43, 'Hic a sequi odit aut voluptas et corrupti.?', 6, 'Difficult', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(52, 38, 'Est sed sunt a voluptate et numquam illum.?', 8, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(53, 40, 'Totam inventore a cumque possimus eum harum voluptate.?', 6, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(54, 42, 'Inventore in pariatur beatae.?', 2, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(55, 40, 'Odio iure culpa et est voluptate.?', 6, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(56, 38, 'Omnis nostrum porro beatae rerum modi cumque harum illo incidunt et suscipit sapiente.?', 8, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(57, 42, 'Explicabo quibusdam eligendi sit minima esse et laboriosam voluptate neque deserunt.?', 8, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(58, 39, 'Velit id soluta nihil dolorum corporis voluptate.?', 6, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(59, 39, 'Tempora voluptate recusandae deserunt et.?', 4, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(60, 38, 'Ipsam voluptatem consequatur.?', 8, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(61, 38, 'Itaque veritatis nihil est quia exercitationem quas quos.?', 2, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(62, 41, 'Sed et omnis explicabo.?', 8, 'Difficult', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(63, 40, 'Voluptatum velit saepe ut enim soluta accusamus.?', 6, 'Moderate', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(64, 40, 'Vel commodi cumque nemo vero doloribus est.?', 8, 'Easy', '2020-07-04 08:52:00', '2020-07-04 08:52:00'),
(65, 42, 'Unde suscipit accusamus rerum ut et explicabo ipsam mollitia autem nostrum.?', 8, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(66, 38, 'Rem consequatur nulla.?', 4, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(67, 39, 'Laudantium adipisci debitis quia saepe officiis ut enim ullam similique.?', 6, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(68, 41, 'Molestias quam ducimus reprehenderit.?', 8, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(69, 38, 'Et numquam omnis facere asperiores molestiae accusamus.?', 4, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(70, 41, 'Voluptas qui vero amet deserunt.?', 2, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(71, 40, 'Exercitationem nemo et placeat quos enim illo nesciunt nemo cum voluptatem in eos voluptas eius laudantium.?', 8, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(72, 43, 'Similique dolore voluptates voluptatem aperiam ipsa nihil.?', 2, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(73, 41, 'Nisi quidem autem id occaecati et rerum.?', 6, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(74, 43, 'Aut inventore dolorum nostrum dolore tempora voluptate et.?', 4, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(75, 38, 'Voluptas rerum iste aut beatae ut at vel fugiat velit et sit.?', 8, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(76, 39, 'Eius ut ut iste animi non magnam nulla pariatur.?', 2, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(77, 42, 'Eveniet porro similique doloremque eveniet doloribus.?', 6, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(78, 40, 'Eum voluptatum iusto fugiat a.?', 6, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(79, 40, 'Corporis voluptatem blanditiis odit voluptatem placeat odio.?', 6, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(80, 42, 'Saepe eveniet inventore dolorum est voluptatem nostrum tenetur dolores tenetur ea.?', 6, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(81, 39, 'Neque corporis aperiam deleniti ipsam tempora consequatur et occaecati alias ea eum.?', 4, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(82, 42, 'Qui aspernatur fugiat id nostrum recusandae.?', 2, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(83, 39, 'Porro assumenda laudantium nesciunt totam.?', 8, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(84, 41, 'Deserunt possimus at iure.?', 2, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(85, 43, 'Recusandae id est quo ratione est exercitationem itaque.?', 6, 'Easy', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(86, 42, 'Aut ut beatae quam saepe quia eligendi quia quisquam a amet et quisquam.?', 4, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(87, 39, 'Cumque quia sit aperiam consequatur quo recusandae sed et et autem nostrum.?', 4, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(88, 40, 'Nam modi repudiandae et repellat.?', 2, 'Difficult', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(89, 38, 'Voluptatibus est ab eum ut et et incidunt blanditiis eaque voluptas quis perspiciatis dolor.?', 4, 'Moderate', '2020-07-04 08:52:01', '2020-07-04 08:52:01'),
(90, 38, 'Quisquam vero et eveniet commodi nam dolores temporibus tempore illo.?', 4, 'Moderate', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(91, 42, 'Amet harum fugiat sunt debitis quibusdam.?', 2, 'Difficult', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(92, 38, 'Eveniet ipsa nemo sequi vel.?', 2, 'Difficult', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(93, 43, 'Dolorem voluptas voluptate aut libero reiciendis.?', 6, 'Easy', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(94, 43, 'Ipsam omnis iure vero maxime neque ut.?', 8, 'Easy', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(95, 42, 'Et exercitationem et et velit nulla quas sit molestiae.?', 6, 'Difficult', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(96, 38, 'Ut reprehenderit assumenda et.?', 2, 'Moderate', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(97, 43, 'Est quos amet soluta rerum.?', 4, 'Difficult', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(98, 42, 'Et numquam ducimus harum id qui velit itaque est ab aliquid eius tenetur blanditiis incidunt est quidem.?', 2, 'Easy', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(99, 41, 'Est a exercitationem eveniet qui et.?', 6, 'Easy', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(100, 43, 'Suscipit culpa ipsam odit iusto et qui est mollitia perferendis aperiam inventore error fuga enim.?', 6, 'Easy', '2020-07-04 08:52:02', '2020-07-04 08:52:02'),
(101, 68, 'iski maa ka????', 2, 'Moderate', NULL, NULL),
(102, 39, 'bc??', 4, 'Easy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sem` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `sem`) VALUES
(1, 'Physics - I', 1),
(2, 'Chemistry - I', 1),
(3, 'Eng. Mechanics', 1),
(4, 'BEL', 1),
(5, 'Physics - II', 2),
(6, 'Chemistry - II', 2),
(7, 'C Programming', 2),
(8, 'Eng. Graphics', 2),
(9, 'DLDA', 3),
(10, 'ECCF', 3),
(11, 'DS', 3),
(13, 'CMS', 2),
(15, 'DBMS', 5),
(16, 'OS', 4),
(17, 'AOA', 4),
(18, 'COA', 4),
(19, 'CG', 4),
(20, 'CN', 5),
(21, 'MUP', 5),
(22, 'TCS', 5),
(23, 'DWM', 6),
(24, 'SE', 6),
(25, 'SPR', 6),
(26, 'CSS', 6),
(27, 'Robotics', 7),
(28, 'AI', 7),
(30, 'DC', 8),
(31, 'NLP', 8);

-- --------------------------------------------------------

--
-- Table structure for table `subject_teacher`
--

CREATE TABLE `subject_teacher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_teacher`
--

INSERT INTO `subject_teacher` (`id`, `subject_id`, `teacher_id`, `created_at`, `updated_at`) VALUES
(1, 22, 14, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(2, 9, 17, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(3, 31, 16, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(4, 17, 6, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(5, 24, 23, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(6, 28, 11, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(7, 27, 7, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(8, 30, 4, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(9, 9, 14, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(10, 4, 21, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(11, 13, 8, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(12, 2, 4, '2020-07-01 17:21:48', '2020-07-01 17:21:48'),
(14, 16, 24, NULL, NULL),
(15, 5, 24, NULL, NULL),
(16, 28, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `doj` date NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` blob DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `email`, `password`, `designation`, `dob`, `address`, `doj`, `gender`, `contact_no`, `profile_image`, `created_at`, `updated_at`) VALUES
(1, 'Muskaan Aswani', 'muskan@gmail.com', '12345', 'hod', '2001-02-10', 'Ulhasnagar, Mumbai', '2020-07-01', 'female', '9881334940', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(2, 'Isha Joglekar', 'isha@gmail.com', '12345', 'hod', '2001-04-13', 'Dombivli, Mumbai', '2020-07-01', 'female', '7045006828', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(3, 'John Doe', 'john@gmail.com', '12345', 'teacher', '2000-11-02', 'Versova, Mumbai', '2020-07-01', 'male', '9022312144', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(4, 'Prof. Jan Keebler IV', 'jeffry14@example.com', '12345', 'teacher', '1996-07-26', 'Esse rerum aspernatur quia omnis.', '2015-11-22', 'others', '+1-568-321-0851', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(5, 'Prof. Andreanne Tremblay DVM', 'kelsi75@example.net', '12345', 'teacher', '1973-07-14', 'Iure occaecati voluptatem dignissimos voluptatem aut aperiam aut. Ex accusamus quidem omnis commodi saepe debitis. Consectetur corrupti aspernatur architecto ipsam.', '2012-08-19', 'female', '1-917-585-7208 x77346', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(6, 'Donato Cormier', 'vandervort.talia@example.org', '12345', 'teacher', '1991-03-30', 'Dolorem aut reprehenderit nobis vel nemo nam voluptas.', '2020-04-27', 'female', '1-904-832-3945 x587', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(7, 'Hassan Herzog', 'orie.thompson@example.org', '12345', 'teacher', '1996-02-22', 'Delectus reprehenderit voluptas amet repudiandae.', '2011-07-14', 'female', '995.836.8458', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(8, 'Ms. Precious Johnson PhD', 'berge.zakary@example.org', '12345', 'teacher', '1991-02-04', 'Possimus minima consequatur et modi quos in recusandae. Sint quas deleniti eos vel. Beatae minus esse et temporibus non.', '2019-09-15', 'female', '+1 (496) 836-5673', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(9, 'Ms. Yvonne Heathcote Sr.', 'esperanza.jakubowski@example.com', '12345', 'teacher', '1970-12-28', 'Quis debitis unde quaerat omnis.', '2011-04-17', 'male', '+1-528-661-3053', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(10, 'Prof. Terrell Schowalter', 'mose.schumm@example.org', '12345', 'teacher', '1980-07-28', 'Id porro blanditiis magnam id blanditiis.', '2017-01-06', 'male', '1-548-251-8188', NULL, '2020-07-01 16:58:33', '2020-07-01 16:58:33'),
(11, 'Cheyenne McKenzie', 'mccullough.watson@example.org', '12345', 'teacher', '1983-03-29', 'Sint modi facilis ea. Corrupti et ea quidem autem autem.', '2018-08-19', 'others', '+1.434.905.2889', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(12, 'Kaylie O\'Reilly', 'esperanza34@example.net', '12345', 'teacher', '1987-11-16', 'Est beatae repellendus quis facilis dolor omnis. Laudantium corrupti ab accusamus saepe et consequatur aperiam distinctio. Tempore voluptas temporibus et hic autem delectus eaque.', '2015-04-17', 'male', '+1-850-494-3000', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(13, 'Prof. Vada Dooley', 'rosella.botsford@example.com', '12345', 'teacher', '1978-12-22', 'Quia ab qui officiis quas itaque ex velit.', '2014-12-09', 'female', '+19125701263', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(14, 'Rosina Wintheiser', 'xwitting@example.net', '12345', 'teacher', '1983-06-10', 'Qui numquam qui quas ullam. Vero sunt itaque tempore ut inventore maiores similique.', '2011-04-26', 'others', '(771) 841-2057 x89583', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(15, 'Jamison Fritsch', 'theodore10@example.org', '12345', 'teacher', '1989-08-27', 'Ullam asperiores mollitia commodi praesentium rem enim.', '2013-11-12', 'female', '1-328-998-4476 x6764', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(16, 'Amber Bailey', 'klein.cody@example.net', '12345', 'teacher', '1983-05-04', 'Et illum sunt dolores dolores repudiandae atque voluptatem. Sint necessitatibus facilis omnis possimus.', '2017-04-24', 'female', '+1-978-252-5161', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(17, 'Carmel Friesen', 'ava97@example.org', '12345', 'teacher', '1981-04-28', 'Voluptas vero maxime officia pariatur similique autem qui officia. Voluptatem voluptatem ratione maxime et corporis quia et non. Itaque sunt qui est quo modi dolorem.', '2012-12-12', 'female', '952.637.7006 x962', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(18, 'Prof. Maymie Larson', 'parmstrong@example.org', '12345', 'teacher', '1983-03-16', 'Harum est nemo esse quibusdam rem voluptas. Deserunt dolores placeat deserunt animi ea iusto. Et doloribus eaque nam iusto qui molestiae iusto.', '2018-11-02', 'others', '+1.901.967.6400', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(19, 'Kathryne Reichert', 'qbreitenberg@example.net', '12345', 'teacher', '1998-11-14', 'Totam omnis quo similique provident provident labore tempore.', '2013-09-23', 'others', '(878) 245-1296', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(20, 'Aurelia Balistreri', 'liza18@example.com', '12345', 'teacher', '2002-03-15', 'Voluptatibus dolores consequatur dolore nemo. Distinctio officiis aut earum libero ex et neque. Occaecati autem voluptate mollitia nemo esse nihil.', '2012-11-24', 'others', '+15689821970', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(21, 'Isabelle O\'Reilly', 'desiree43@example.net', '12345', 'teacher', '1990-06-09', 'Neque eveniet aut perspiciatis blanditiis quam. Consequuntur optio aut accusamus sunt explicabo officiis.', '2020-02-28', 'male', '380-801-5771 x556', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(22, 'Karl Hane', 'nathanael.schneider@example.net', '12345', 'teacher', '1976-09-28', 'Dolorem ea enim qui quia iste sit iure. Saepe autem animi maiores fuga non voluptatem.', '2012-11-16', 'others', '258-889-5405 x96256', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(23, 'Mrs. Rosella Rohan I', 'marcelle95@example.com', '12345', 'teacher', '1982-05-24', 'Nulla neque eligendi tempore quidem officiis architecto nisi.', '2011-09-21', 'female', '(837) 834-4888', NULL, '2020-07-01 16:58:34', '2020-07-01 16:58:34'),
(24, 'Jane Doe', 'jane@gmail.com', '12345', 'teacher', '1993-07-12', 'Andheri', '2020-07-01', 'female', '9922368081', 0xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108004c008003012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f772a69369a90d25513623db46da7d1405866da36d3a8a2e0371498a751400da4a751400da4a71200a8a6b982dc6679a38c75f9d80a0075142ba3a8656041e41069dc7b53b80dc518aad71a85bc04a6edf2060bb17d48cf5e9d39a60d5600de5cb98e5c062879c03c0c9e8327a6680b1a45d738dc33f5a370f5a4da029e001deb323d5ad6ef7fd86496e5518ab34006d07d373707f02690cd3dc3d69378aad11bb6753208963c72392c4fd7a0ab1b73400798319cf146fc8c8e94bb4526d3400d2e7d0fe546e6eca4d388a31ef400d61291f2819f434e11315c3390d8eabc7f3cd183eb460fad005792cc4971be596423180807ca3dfeb4f8ecada13bd51bcc2397c9dcdf53d7bd4d96f5a013dd8d202b9f3595f0265032154e32fefed48913b291229542303e63b89fc6ac664cf2c0fe069a7cdc70e338e98e29815deda06751b4e108c3127db8fcc0a636d470121660bc1c26738e9fd6ac85971f3bab7d1714a51b6e15c83eb8a00b3cfa8aae2c6d15d9d6da1576e59950027f115629323d290111b78c8c664fc2461fd69bf638c9fbf37fdfe7ff1a98b7a0a6991876a0085ac623d5e71f49dff00c6aacf6202922faf2219032b2038c9c7706accf7715bc4d2dc4c91a0ea5c802bcd7c51f15eca2825b6d13f7d31e04ce9f203edeb46bd00778eb5bd77c232db3d9ebad3c73120c53471175f7fbbc8ae663f8a7e238800f3c127a17887f4c5717797d79aade35e5fcef3cadd59cff9c0aad2bb3102b44b4d44cf4c83e31ea8814dc69d6b2e383b19933fceb62d7e33697232fdaf4fba83d7ca2b20fe87f4af205cc8e22eeff28c7af6ebef555b703d4fd28714099f46e9df10fc33a9ed58b538a3918e364f98ce7f1e3f5ae8a2b81202dc6dcfca7b30f5af934f38e4e6b4b4bf136b9a1b03a7ea571128ff009665b721ff00809e3f4a96867d481c1e841fc69775789e91f19ae230b16b1a724c00e65b73b189f70783fa576ba2fc47d075991a389eead9d4676cf1f5fa1526901db6f1e8693cc5ce391f5a85246740d19f3148c82bcd1e7e3a8a00b4cf8a69627d6a39e68e046924654451c963d0562cfe2069c6dd32212e7fe5b48084fc3b9a12036a59e3850bcd2a46a3a9638ae23c4ff112cf49c436aa6499d7e57c64b0f551e9ee78fad2dfcf05a44f77ac5e199a3f98e54ed5ff00808feb9af2ff0015789a3d66e0a5b460443f8c0dbbbebc64fe3f95349015b5ef156a9aec84dd4efb339f28703f1ac5487f89c1c5491845396319f625bfa55d4fb1aa1927b885dc748d439fc3a0fe756912d99ec477071d80a6c71ee7c9071562e278ee1d42e17d8478fea6adc5e5200a7a0fee8c1aa48572b1b5654f35411b4e41ddd2a3d42d7ecda8cd164b0c860cddc11907f5abf7372bf6393f7f20f9b8524927f5acfbc5bb6f2ae2e5837991aec27192a381fca8605765d8f8618a59106c0cbf303f98a9a48e496d3cc0a309d4e474aa49315e0301f852d862140c718152595c258dfc33bc7e6a46e18a072bbb1ef51f98ec7ef134c63b873d7e94580f79f08f8df4dd5d05b5b19609d07faa9df713fee9c924576ab7c255db2aeefa75af94ede696d6e5278243148841561dabd13c3ff13ae2d82c3aa8f317a0993191f5a8711a67792f8a343ba557bad5ada621b852c0283ecbfd4d666a3e3fd22d2026d9fed32745487bfe24600af1eefe9c668524b1e48c7a5161dcddd6757d5fc472e655296e0e5215e02fb9f53ee6b15e23112180ce7b73fca94cae9f229c03d6a191982eecf26ab444ea383b7217033c74a6b48e8701db9f438a8892509ef51b31073dc77a130b16a203049e73d326a6ced4391d7d0f6aab1b7f3a5766e9bdb19f5aa105ccb85d838fad6add42f73a069d296550aae80f7fbc6b018ee619efd7f2ad5763ff0008c5bf3f767602a6e3b1556678c328c153c1181c8aace8c492aad8ef4d495b8e8723b8152099d40c11cf07814c447e6b23641e69048cadbce067da98ff00c473d05478c138f4f4a571d8b4aace3394c1f7c53d6070321578ee18647eb50413387e36fdec7dd15764fddc84ae413df354847fffd9, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subject__teachers_subject_id_teacher_id_unique` (`subject_id`,`teacher_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teachers_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
