
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class MySQLConnect {
    Connection conn;
    
    public static Connection connectionDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/automatic_question_paper_generator","root","");
            //JOptionPane.showMessageDialog(null,"Connection established successfully!");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Connection Failed!" + e);
             return null;  
        }
    }
}
