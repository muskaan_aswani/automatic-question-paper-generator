
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class AdminPanel extends javax.swing.JFrame {

    /**
     * Creates new form AdminPanel
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public AdminPanel() {
        initComponents();
        setResizable(false);
        conn = MySQLConnect.connectionDB();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2 - getWidth()/2 , dimension.height/2 - getHeight()/2);
        String sql = "select subjects.name ,GROUP_CONCAT(teachers.name) as techers_name from teachers inner join subject_teacher on subject_teacher.teacher_id = teachers.id right outer join subjects on subject_teacher.subject_id = subjects.id group by subjects.name";
        updateTableData(sql);
    
    }
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tableSubjectTeacher.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
            }catch(SQLException e){
            JOptionPane.showMessageDialog(this,"Error while fetching data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing rs or ps");
            }
        }
    }
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(),nCol = dtm.getColumnCount();
        Object[] [] tableData = new Object[nRow][nCol];
        for(int i = 0 ; i < nRow ; i++)
            for(int j = 0 ; j < nCol ; j++)
                tableData[i][j] = dtm.getValueAt(i,j);
        String[] colHeads = {"Subject Name","Teacher Name"};
        DefaultTableModel myModel = new DefaultTableModel(tableData,colHeads){
            @Override
            public boolean isCellEditable(int row,int column){
                return false;
            }
        };
        return myModel;
    }
    private void getTeachers(){
        try{
            if(cmbTeacher.getItemCount() > 0){
                    for(int i=cmbTeacher.getItemCount()-1;i>0;i--){
                        cmbTeacher.removeItemAt(i);
                    }
            }
            
            String sql = "SELECT name FROM teachers WHERE designation=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,"teacher");
            rs = ps.executeQuery();
            if(rs.next() == false){
                if(cmbTeacher.getItemCount()-1 >= 0)
                    cmbTeacher.removeItemAt(0);   
            }else{
                do{
                cmbTeacher.addItem(rs.getString("name"));
            }while(rs.next());
            }
            if(cmbTeacher.getItemCount() > 1){
                    cmbTeacher.removeItemAt(0);
//                    cmbTeacher.setSelectedIndex(0);
            }
            
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
        } 
    }
    private void getTeacherForSubject(int subject_id){
        try{
           if(cmbTeacher1.getItemCount() > 0){
                    for(int i=cmbTeacher1.getItemCount()-1;i>0;i--){
                        cmbTeacher1.removeItemAt(i);
                    }
                }
            String sql = "SELECT teachers.name FROM teachers WHERE teachers.id in ( SELECT teacher_id FROM subject_teacher WHERE subject_id = ? )";
            ps = conn.prepareStatement(sql);
            ps.setInt(1,subject_id);
            rs = ps.executeQuery();
            
            if(rs.next() == false){
             if(cmbTeacher1.getItemCount()-1 >= 0)
                    cmbTeacher1.removeItemAt(0);   
            }
            else{
                do{
                    System.out.println(rs.getString("name"));
                    cmbTeacher1.addItem(rs.getString("name"));
                }while(rs.next());
            }
            if(cmbTeacher1.getItemCount() > 1){
                    cmbTeacher1.removeItemAt(0);
//                    cmbTeacher1.setSelectedIndex(0);
                }
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
        } 
    }
    
    private void getSubjects(int semValue,boolean flag){
        try{
            if(flag){
                if(cmbSubject.getItemCount() > 0){
                    for(int i=cmbSubject.getItemCount()-1;i>0;i--){
                        cmbSubject.removeItemAt(i);
                    }
                }
             String sql = "SELECT name FROM subjects where sem=?";
                ps = conn.prepareStatement(sql);
                ps.setInt(1,semValue);
                rs = ps.executeQuery();
                if(rs.next() == false){
                    if(cmbSubject.getItemCount()-1 >= 0)
                    cmbSubject.removeItemAt(0);
                }
                else{
                    do{       
                        cmbSubject.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                if(cmbSubject.getItemCount() > 1){
                    cmbSubject.removeItemAt(0);
                    cmbSubject.setSelectedIndex(0);
                }
            }
            else{
//                System.out.println("hi");
                if(cmbSubject1.getItemCount() > 0){
                    for(int i=cmbSubject1.getItemCount()-1;i>0;i--){
                        cmbSubject1.removeItemAt(i);
                    }
                }
             String sql = "SELECT name FROM subjects where sem=?";
                ps = conn.prepareStatement(sql);
                ps.setInt(1,semValue);
                rs = ps.executeQuery();
                if(rs.next() == false){
                    if(cmbSubject1.getItemCount()-1 >= 0)
                    cmbSubject1.removeItemAt(0);
                }
                else{
                    do{       
                        cmbSubject1.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                if(cmbSubject1.getItemCount() > 1){
                    cmbSubject1.removeItemAt(0);
                    cmbSubject1.setSelectedIndex(0);
                }
                
            }
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
        }
    }
    private int getTeacherID(String name){
        try{
        String sql = "SELECT id from teachers where name=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("id");
            }
            return -1;
        }
        catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
                 return -1;
        }
    }
    private int getSubjectID(String name){
        try{
        String sql = "SELECT id from subjects where name=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("id");
            }
            return -1;
        }
        catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
                 return -1;
        }
    }
    private void clearfields(){
        txtSubject.setText("");
        cmbSem.setEnabled(false);
        cmbSem.setSelectedIndex(0);
        cmbSem1.setSelectedIndex(0);
        cmbSubject.setSelectedIndex(0);
        cmbTeacher.setSelectedIndex(-1);
        cmbSem2.setSelectedIndex(0);
        cmbSubject1.setSelectedIndex(0);
        cmbTeacher1.setSelectedIndex(-1);
        cmbSubject.setEnabled(false);
        cmbTeacher.setEnabled(false);
        cmbSubject1.setEnabled(false);
        cmbTeacher1.setEnabled(false);
        btnAssign1.setEnabled(false);
        btnAssign.setEnabled(false);
        btnSubject.setEnabled(false);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtSubject = new javax.swing.JTextField();
        cmbSem = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableSubjectTeacher = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmbSubject = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        cmbTeacher = new javax.swing.JComboBox<>();
        btnSubject = new javax.swing.JButton();
        btnAssign = new javax.swing.JButton();
        btnLogOut = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cmbSem1 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cmbSem2 = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        cmbSubject1 = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cmbTeacher1 = new javax.swing.JComboBox<>();
        btnAssign1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/knowledge.png"))); // NOI18N

        jLabel2.setText("Add Subject :");

        txtSubject.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        txtSubject.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSubjectFocusGained(evt);
            }
        });
        txtSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubjectActionPerformed(evt);
            }
        });

        cmbSem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8" }));
        cmbSem.setToolTipText("");
        cmbSem.setEnabled(false);
        cmbSem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSemActionPerformed(evt);
            }
        });

        jLabel3.setText("Select Sem :");

        tableSubjectTeacher.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Subjects", "Teachers"
            }
        ));
        jScrollPane3.setViewportView(tableSubjectTeacher);

        jLabel4.setText("Assign Subject To Teacher :");

        jLabel5.setText("Select Subject :");

        cmbSubject.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbSubject.setSelectedIndex(-1);
        cmbSubject.setToolTipText("");
        cmbSubject.setEnabled(false);
        cmbSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectActionPerformed(evt);
            }
        });

        jLabel6.setText("Select Teacher :");

        cmbTeacher.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbTeacher.setSelectedIndex(-1);
        cmbTeacher.setToolTipText("");
        cmbTeacher.setEnabled(false);
        cmbTeacher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTeacherActionPerformed(evt);
            }
        });

        btnSubject.setBackground(new java.awt.Color(51, 51, 51));
        btnSubject.setForeground(new java.awt.Color(255, 255, 255));
        btnSubject.setText("Add");
        btnSubject.setEnabled(false);
        btnSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubjectActionPerformed(evt);
            }
        });

        btnAssign.setBackground(new java.awt.Color(51, 51, 51));
        btnAssign.setForeground(new java.awt.Color(255, 255, 255));
        btnAssign.setText("Assign");
        btnAssign.setEnabled(false);
        btnAssign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssignActionPerformed(evt);
            }
        });

        btnLogOut.setBackground(new java.awt.Color(51, 51, 51));
        btnLogOut.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        btnLogOut.setForeground(new java.awt.Color(255, 255, 255));
        btnLogOut.setText("Log Out");
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        jLabel7.setText("Select Sem :");

        cmbSem1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8" }));
        cmbSem1.setToolTipText("");
        cmbSem1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cmbSem1FocusGained(evt);
            }
        });
        cmbSem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbSem1MouseClicked(evt);
            }
        });
        cmbSem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSem1ActionPerformed(evt);
            }
        });

        jLabel8.setText("Remove Teacher From Subject :");

        jLabel9.setText("Select Sem :");

        cmbSem2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8" }));
        cmbSem2.setToolTipText("");
        cmbSem2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cmbSem2FocusGained(evt);
            }
        });
        cmbSem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbSem2MouseClicked(evt);
            }
        });
        cmbSem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSem2ActionPerformed(evt);
            }
        });

        jLabel10.setText("Select Subject :");

        cmbSubject1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbSubject1.setSelectedIndex(-1);
        cmbSubject1.setToolTipText("");
        cmbSubject1.setEnabled(false);
        cmbSubject1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubject1ActionPerformed(evt);
            }
        });

        jLabel11.setText("Select Teacher :");

        cmbTeacher1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbTeacher1.setSelectedIndex(-1);
        cmbTeacher1.setToolTipText("");
        cmbTeacher1.setEnabled(false);
        cmbTeacher1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTeacher1ActionPerformed(evt);
            }
        });

        btnAssign1.setBackground(new java.awt.Color(51, 51, 51));
        btnAssign1.setForeground(new java.awt.Color(255, 255, 255));
        btnAssign1.setText("Remove");
        btnAssign1.setEnabled(false);
        btnAssign1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssign1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnSubject))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel6)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cmbTeacher, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnAssign)))
                        .addGap(12, 12, 12))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(220, 220, 220)
                        .addComponent(btnLogOut))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(8, 8, 8)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(cmbSem1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(cmbSem, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jLabel3))
                                        .addGap(27, 27, 27)
                                        .addComponent(jLabel5))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cmbSem2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addComponent(cmbSubject1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(cmbTeacher1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnAssign1)))))
                        .addGap(0, 3, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogOut))
                .addGap(25, 25, 25)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSubject)
                            .addComponent(cmbSem, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbSem1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTeacher, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAssign))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbSem2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbSubject1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTeacher1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAssign1))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbSemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSemActionPerformed
       btnSubject.setEnabled(true);
    }//GEN-LAST:event_cmbSemActionPerformed

    private void cmbSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectActionPerformed
        cmbTeacher.setEnabled(true);
        getTeachers();
    }//GEN-LAST:event_cmbSubjectActionPerformed

    private void txtSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubjectActionPerformed
       
    }//GEN-LAST:event_txtSubjectActionPerformed

    private void btnSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubjectActionPerformed
       if(cmbSem.getSelectedItem()!=null && !txtSubject.getText().equals("") ){
        String sql = "INSERT INTO subjects(name,sem) VALUES(?, ?)";
        String name = txtSubject.getText();
        int sem = Integer.parseInt(cmbSem.getSelectedItem().toString());
        
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            ps.setInt(2,sem);
            ps.execute();
           clearfields();
           String loadTableQuery = "select subjects.name ,GROUP_CONCAT(teachers.name) as techers_name from teachers inner join subject_teacher on subject_teacher.teacher_id = teachers.id right outer join subjects on subject_teacher.subject_id = subjects.id group by subjects.name";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this,"Subject Added Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Some Issue : " + ex);
        }
       }
    }//GEN-LAST:event_btnSubjectActionPerformed

    private void btnAssignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssignActionPerformed
       if(cmbSubject.getSelectedItem() != null && cmbTeacher.getSelectedItem() != null && cmbSem1.getSelectedItem()!=null ){
        try{
         String sql = "INSERT into subject_teacher(subject_id,teacher_id) VALUES(?, ?)";
            int subject_id = getSubjectID(cmbSubject.getSelectedItem().toString());
            int teacher_id = getTeacherID(cmbTeacher.getSelectedItem().toString());
            ps = conn.prepareStatement(sql);
            ps.setInt(1,subject_id);
            ps.setInt(2,teacher_id);
            ps.execute();
            clearfields();
            String loadTableQuery = "select subjects.name ,GROUP_CONCAT(teachers.name) as techers_name from teachers inner join subject_teacher on subject_teacher.teacher_id = teachers.id right outer join subjects on subject_teacher.subject_id = subjects.id group by subjects.name";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this,"Subject Assigned Successfully!");
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
        }
       }
    }//GEN-LAST:event_btnAssignActionPerformed

    private void cmbSem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSem1ActionPerformed
        cmbSubject.setEnabled(true);
        getSubjects(Integer.parseInt(cmbSem1.getSelectedItem().toString()),true);
        
    }//GEN-LAST:event_cmbSem1ActionPerformed

    private void cmbTeacherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTeacherActionPerformed
        btnAssign.setEnabled(true);
    }//GEN-LAST:event_cmbTeacherActionPerformed

    private void txtSubjectFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSubjectFocusGained
        cmbSem.setEnabled(true);
    }//GEN-LAST:event_txtSubjectFocusGained

    private void cmbSem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSem1MouseClicked
        //cmbSubject.removeAllItems();
        
    }//GEN-LAST:event_cmbSem1MouseClicked

    private void cmbSem1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cmbSem1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSem1FocusGained

    private void cmbSem2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cmbSem2FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSem2FocusGained

    private void cmbSem2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSem2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSem2MouseClicked

    private void cmbSem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSem2ActionPerformed
        cmbSubject1.setEnabled(true);
        getSubjects(Integer.parseInt(cmbSem2.getSelectedItem().toString()),false);
    }//GEN-LAST:event_cmbSem2ActionPerformed

    private void cmbSubject1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubject1ActionPerformed
        if(cmbSubject1.getSelectedItem() != null)
            getTeacherForSubject(getSubjectID(cmbSubject1.getSelectedItem().toString()));
        cmbTeacher1.setEnabled(true);
    }//GEN-LAST:event_cmbSubject1ActionPerformed

    private void cmbTeacher1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTeacher1ActionPerformed
        System.out.println(getSubjectID(cmbSubject1.getSelectedItem().toString()));
        
        btnAssign1.setEnabled(true);
    }//GEN-LAST:event_cmbTeacher1ActionPerformed

    private void btnAssign1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssign1ActionPerformed
       if(cmbSubject1.getSelectedItem() != null && cmbTeacher1.getSelectedItem() != null && cmbSem2.getSelectedItem()!=null ){
        try{
            String sql = "DELETE FROM subject_teacher where subject_id = ? and teacher_id = ?";
            int subject_id = getSubjectID(cmbSubject1.getSelectedItem().toString());
            int teacher_id = getTeacherID(cmbTeacher1.getSelectedItem().toString());
            ps = conn.prepareStatement(sql);
            ps.setInt(1,subject_id);
            ps.setInt(2,teacher_id);
            ps.execute();
            clearfields();
            String loadTableQuery = "select subjects.name ,GROUP_CONCAT(teachers.name) as techers_name from teachers inner join subject_teacher on subject_teacher.teacher_id = teachers.id right outer join subjects on subject_teacher.subject_id = subjects.id group by subjects.name";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this,"Teacher Removed From Subject Successfully!");
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
        }
       }
    }//GEN-LAST:event_btnAssign1ActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnLogOutActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminPanel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAssign;
    private javax.swing.JButton btnAssign1;
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnSubject;
    private javax.swing.JComboBox<String> cmbSem;
    private javax.swing.JComboBox<String> cmbSem1;
    private javax.swing.JComboBox<String> cmbSem2;
    private javax.swing.JComboBox<String> cmbSubject;
    private javax.swing.JComboBox<String> cmbSubject1;
    private javax.swing.JComboBox<String> cmbTeacher;
    private javax.swing.JComboBox<String> cmbTeacher1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tableSubjectTeacher;
    private javax.swing.JTextField txtSubject;
    // End of variables declaration//GEN-END:variables
}
