/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
public class TeacherPanel extends javax.swing.JFrame {

    
    /**
     * Creates new form TeacherPanel
     */
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection conn = null;
    public TeacherPanel(int teacher_id) {
        
        initComponents();
        setResizable(false);
        
        conn = MySQLConnect.connectionDB();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2 - getWidth()/2 , dimension.height/2 - getHeight()/2);
        teacher_ID = teacher_id;
        getTeacherDetails();
        getSubjects(teacher_ID);
        flag = true;
        if(cmbSubjects.getSelectedItem() == null)
        {
            btnUnit.setEnabled(false);
            btnCustomize.setEnabled(false);
            btnSemester.setEnabled(false);
        }
         
    }
    
    public void getSubjects(int teacher_id){
         
        try {
            if(cmbSubjects.getItemCount() > 0){
                    for(int i=cmbSubjects.getItemCount()-1;i>=0;i--){
                        cmbSubjects.removeItemAt(i);
                    }
            }
             String sql = "SELECT  name from subjects where id in (select subject_id from subject_teacher where teacher_id = ?)";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,teacher_id);
             rs = ps.executeQuery();
             
             if(rs.next() == false){
                    if(cmbSubjects.getItemCount()-1 >= 0)
                    cmbSubjects.removeItemAt(0);
                }
                else{
                    do{       
                        cmbSubjects.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }         
    }
    public void getChapters(){
         try {
             int subject_id = getSubjectID(cmbSubjects.getSelectedItem().toString());
//             System.out.println(subject_id);

            if(cmbExistingChapters.getItemCount() > 0){
                    for(int i=cmbExistingChapters.getItemCount()-1;i>=0;i--){
                        cmbExistingChapters.removeItemAt(i);
                    }
                }
             String sql = "SELECT  name from chapters where subject_id = ?";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,subject_id);
             rs = ps.executeQuery();
             
             if(rs.next() == false){
                    if(cmbExistingChapters.getItemCount()-1 >= 0)
                    cmbExistingChapters.removeItemAt(0);
                }
                else{
                    do{       
                        cmbExistingChapters.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
     private int getSubjectID(String name){
        try{
        String sql = "SELECT id from subjects where name= ? ";
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            rs = ps.executeQuery();
            if(rs.next()){
//                System.out.println(rs.getInt("id"));
                return rs.getInt("id");
            }
            return -1;
        }
        catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
                 return -1;
        }
    }
     public int getChapterId(String name){
         try {
             
             
             String sql = "Select id from chapters where name = ?";
             ps = conn.prepareStatement(sql);
             ps.setString(1,name);
             
             rs = ps.executeQuery();
             if(rs.next()){
                 return rs.getInt("id");
             }
             return -1;
         } catch (SQLException ex) {
            
              JOptionPane.showMessageDialog(this,"Some Issue : " + ex);
               return -1;
         }
     }
    public void getTeacherDetails(){
        
         try {
             String sql = "SELECT name, profile_image from teachers where id = ?";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,teacher_ID);
             rs = ps.executeQuery();
             if(rs.next()){
                 if(rs.getBytes("profile_image")!=null){
                    byteImage = rs.getBytes("profile_image");
                    BufferedImage image =ImageIO.read(new ByteArrayInputStream(byteImage));
                    ImageIcon icon = new ImageIcon(image);
                    lblImage.setIcon(icon);
                 }
                txtTeacherName.setText(rs.getString("name"));
                 
             }
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Some Issue : " + ex);
         } catch (IOException ex) {
             JOptionPane.showMessageDialog(null,"Some Issue : " + ex);
         }
    }
    
    public void removeChapter(int cid){
         try {
             String sql = "DELETE FROM chapters where id = ?";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,cid);
             ps.execute();
             
             JOptionPane.showMessageDialog(this,"Removed Successfully!");
         } catch (SQLException ex) {
              JOptionPane.showMessageDialog(null,"Some Issue : " + ex);
         }
        
    }
    
    public void clearfields(){
        cmbSubjects.setSelectedIndex(0);
        cmbExistingChapters.setSelectedIndex(-1);
         cmbExistingChapters.setEnabled(false);
        
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        imageTeacher = new javax.swing.JDesktopPane();
        lblImage = new javax.swing.JLabel();
        txtTeacherName = new javax.swing.JLabel();
        btnModifyQuestions = new javax.swing.JButton();
        cmbSubjects = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbExistingChapters = new javax.swing.JComboBox<>();
        btnAddChapter = new javax.swing.JButton();
        btnRemoveChapter = new javax.swing.JButton();
        btnEditChapter = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        btnUnit = new javax.swing.JButton();
        btnSemester = new javax.swing.JButton();
        btnCustomize = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jPanel1FocusLost(evt);
            }
        });

        imageTeacher.setBackground(new java.awt.Color(153, 153, 153));
        imageTeacher.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        lblImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        imageTeacher.setLayer(lblImage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout imageTeacherLayout = new javax.swing.GroupLayout(imageTeacher);
        imageTeacher.setLayout(imageTeacherLayout);
        imageTeacherLayout.setHorizontalGroup(
            imageTeacherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imageTeacherLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        imageTeacherLayout.setVerticalGroup(
            imageTeacherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imageTeacherLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtTeacherName.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        txtTeacherName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnModifyQuestions.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnModifyQuestions.setText("Modify Questions");
        btnModifyQuestions.setEnabled(false);
        btnModifyQuestions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyQuestionsActionPerformed(evt);
            }
        });

        cmbSubjects.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cmbSubjectsFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                cmbSubjectsFocusLost(evt);
            }
        });
        cmbSubjects.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbSubjectsMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cmbSubjectsMouseExited(evt);
            }
        });
        cmbSubjects.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectsActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel1.setText("Choose a Subject to Add or Delete Chapters : ");

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel2.setText("Existing Chapters : ");

        cmbExistingChapters.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        cmbExistingChapters.setFocusCycleRoot(true);
        cmbExistingChapters.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cmbExistingChaptersFocusGained(evt);
            }
        });
        cmbExistingChapters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbExistingChaptersActionPerformed(evt);
            }
        });

        btnAddChapter.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnAddChapter.setText("Add Chapter");
        btnAddChapter.setEnabled(false);
        btnAddChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddChapterActionPerformed(evt);
            }
        });

        btnRemoveChapter.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnRemoveChapter.setText("Remove Chapter");
        btnRemoveChapter.setEnabled(false);
        btnRemoveChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveChapterActionPerformed(evt);
            }
        });

        btnEditChapter.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnEditChapter.setText("Edit Chapter");
        btnEditChapter.setEnabled(false);
        btnEditChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditChapterActionPerformed(evt);
            }
        });

        btnLogout.setBackground(new java.awt.Color(51, 51, 51));
        btnLogout.setForeground(new java.awt.Color(255, 255, 255));
        btnLogout.setText("Log Out");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(250, 250, 250));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnUnit.setBackground(new java.awt.Color(255, 255, 255));
        btnUnit.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnUnit.setForeground(new java.awt.Color(51, 51, 51));
        btnUnit.setText("Unit Test");
        btnUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnitActionPerformed(evt);
            }
        });

        btnSemester.setBackground(new java.awt.Color(255, 255, 255));
        btnSemester.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnSemester.setForeground(new java.awt.Color(51, 51, 51));
        btnSemester.setText("Semester");
        btnSemester.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSemesterActionPerformed(evt);
            }
        });

        btnCustomize.setBackground(new java.awt.Color(255, 255, 255));
        btnCustomize.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        btnCustomize.setForeground(new java.awt.Color(51, 51, 51));
        btnCustomize.setText("Customize");
        btnCustomize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomizeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(btnUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(btnSemester, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addComponent(btnCustomize, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSemester, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCustomize, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 13)); // NOI18N
        jLabel3.setText("Generate Question Paper  :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(199, 199, 199)
                                .addComponent(jLabel3))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(btnAddChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(24, 24, 24)
                                    .addComponent(cmbExistingChapters, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(44, 44, 44)
                                    .addComponent(btnEditChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(35, 35, 35)
                                .addComponent(cmbSubjects, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnRemoveChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnModifyQuestions))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(221, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(imageTeacher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(212, 212, 212)
                        .addComponent(btnLogout)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(txtTeacherName, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(210, 210, 210))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnLogout))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(imageTeacher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(txtTeacherName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbSubjects, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(btnEditChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbExistingChapters, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModifyQuestions, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSemesterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSemesterActionPerformed
        // TODO add your handling code here:te
        this.setVisible(false);
        new SemesterPaper(teacher_ID).setVisible(true);
    }//GEN-LAST:event_btnSemesterActionPerformed

    private void btnRemoveChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveChapterActionPerformed
        if(cmbExistingChapters.getItemCount()> 0 ){
            int a=JOptionPane.showConfirmDialog(this,"Are you sure?");  
        if(a==JOptionPane.YES_OPTION){  
            
               int cid = getChapterId(cmbExistingChapters.getSelectedItem().toString());
               removeChapter(cid);
               clearfields();
         }else if(a==JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);  
         }
        }else{
            JOptionPane.showMessageDialog(this,"No Chapters to be removed");
        }
    
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRemoveChapterActionPerformed

    private void btnCustomizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomizeActionPerformed
        // TODO add your handling code here:
        new CustomizePaper(teacher_ID).setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnCustomizeActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnEditChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditChapterActionPerformed
        // TODO add your handling code here:
         if(cmbExistingChapters.getItemCount()> 0 ){
             new EditChapter(getChapterId(cmbExistingChapters.getSelectedItem().toString())).setVisible(true);
         }else{
             JOptionPane.showMessageDialog(this,"No Chapters to be Edited");
         }
        
            
    }//GEN-LAST:event_btnEditChapterActionPerformed

    private void btnAddChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddChapterActionPerformed
        // TODO add your handling code here:
        new AddChapter(getSubjectID(cmbSubjects.getSelectedItem().toString())).setVisible(true);
        clearfields();
    }//GEN-LAST:event_btnAddChapterActionPerformed

    private void cmbExistingChaptersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbExistingChaptersActionPerformed
        // TODO add your handling code here:
        btnEditChapter.setEnabled(true);
        btnRemoveChapter.setEnabled(true);
        btnAddChapter.setEnabled(true);
        btnModifyQuestions.setEnabled(true);
        
       
    }//GEN-LAST:event_cmbExistingChaptersActionPerformed

    private void cmbSubjectsFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cmbSubjectsFocusGained
        // TODO add your handling code here:
     
    }//GEN-LAST:event_cmbSubjectsFocusGained

    private void cmbSubjectsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectsActionPerformed
        // TODO add your handling code here:
        if(flag){
            getChapters();
            cmbExistingChapters.setEnabled(true);
        }
         
    }//GEN-LAST:event_cmbSubjectsActionPerformed

    private void jPanel1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jPanel1FocusLost
        // TODO add your handling code here:
      
    }//GEN-LAST:event_jPanel1FocusLost

    private void cmbSubjectsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubjectsMouseClicked
        // TODO add your handling code here:
         
    }//GEN-LAST:event_cmbSubjectsMouseClicked

    private void cmbExistingChaptersFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cmbExistingChaptersFocusGained
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_cmbExistingChaptersFocusGained

    private void cmbSubjectsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubjectsMouseExited
        // TODO add your handling code here:
         
    }//GEN-LAST:event_cmbSubjectsMouseExited

    private void cmbSubjectsFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cmbSubjectsFocusLost
        // TODO add your handling code here:
        
    }//GEN-LAST:event_cmbSubjectsFocusLost

    private void btnModifyQuestionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyQuestionsActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new ModifyQuestions(getChapterId(cmbExistingChapters.getSelectedItem().toString())).setVisible(true);
        
    }//GEN-LAST:event_btnModifyQuestionsActionPerformed

    private void btnUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnitActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new UnitTest(teacher_ID).setVisible(true);
    }//GEN-LAST:event_btnUnitActionPerformed

   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TeacherPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TeacherPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TeacherPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TeacherPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TeacherPanel(teacher_ID).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChapter;
    private javax.swing.JButton btnCustomize;
    private javax.swing.JButton btnEditChapter;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnModifyQuestions;
    private javax.swing.JButton btnRemoveChapter;
    private javax.swing.JButton btnSemester;
    private javax.swing.JButton btnUnit;
    private javax.swing.JComboBox<String> cmbExistingChapters;
    private javax.swing.JComboBox<String> cmbSubjects;
    private javax.swing.JDesktopPane imageTeacher;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel txtTeacherName;
    // End of variables declaration//GEN-END:variables
    private byte[] byteImage;
    public static int teacher_ID;
    private boolean flag = false;
}
