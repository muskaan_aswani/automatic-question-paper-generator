
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Isha
 */
public class CustomizePaper extends javax.swing.JFrame {

    /**
     * Creates new form CustomizePaper
     */
     Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection conn = null;
    public CustomizePaper(int teacher_id) {
        initComponents();
        setResizable(false);
        conn = MySQLConnect.connectionDB();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2 - getWidth()/2 , dimension.height/2 - getHeight()/2);
        this.teacher_id = teacher_id;
        getSubjects(teacher_id);
        cmbSubjects.setSelectedIndex(0);
        cmbMarks.setSelectedIndex(-1);
        flag2 = true;
    }

       
    public void getSubjects(int teacher_id){
         
        try {
            if(cmbSubjects.getItemCount() > 0){
                    for(int i=cmbSubjects.getItemCount()-1;i>=0;i--){
                        cmbSubjects.removeItemAt(i);
                    }
            }
             String sql = "SELECT  name from subjects where id in (select subject_id from subject_teacher where teacher_id = ?)";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,teacher_id);
             rs = ps.executeQuery();
             
             if(rs.next() == false){
                    if(cmbSubjects.getItemCount()-1 >= 0)
                    cmbSubjects.removeItemAt(0);
                }
                else{
                    do{       
                        cmbSubjects.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }         
    }
    
   
    private int getSubjectID(String name){
        try{
        String sql = "SELECT id from subjects where name= ? ";
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            rs = ps.executeQuery();
            if(rs.next()){
//                System.out.println(rs.getInt("id"));
                return rs.getInt("id");
            }
            return -1;
        }
        catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
                 return -1;
        }
    }
    
  
    public int[] getChapterId(String names[],boolean flag){
        
        int count = names.length;
        int[] ids = new int[count];
         try {
             String sql = "Select id from chapters where name in(";
             for(int i=0;i<count;i++)
             {    
                 sql += "'";
                 sql += names[i]+"',";
             }
             sql = sql.substring(0, sql.length() - 1);
             sql += ")";
             ps = conn.prepareStatement(sql);
             
             rs = ps.executeQuery();
             int i=0;
             while(rs.next()){
                 ids[i] = rs.getInt("id");
                 i++;
             }
             return ids;
         } catch (SQLException ex) {
            
              JOptionPane.showMessageDialog(this,"Some Issue : " + ex);
               return ids;
         }
     }
  
    public void getChapters(){
         try {
             int subject_id = getSubjectID(cmbSubjects.getSelectedItem().toString());

            listChapter.removeAll();
             
             String sql = "SELECT  name from chapters where subject_id = ?";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,subject_id);
             rs = ps.executeQuery();
             if(rs.next() == false){
      
                }
                else{
                    
                    do{    
                        
                        listChapter.add(rs.getString("name"));
                    }while(rs.next());
                    //listChapter.select(-1);
                }
             
             //listChapter.setSelectedIndex(-1);
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    private void generatePaper(){
      
        try {
            //Select name,marks from questions where (level in("Easy","Moderate")) and chapter_id in(38,42) and marks in(2,4)
            Map<String,Integer> questions = new HashMap<>();
            Map<String,Integer> finalQuestions = new HashMap<>();
            
            
            
            String[] chapNames = new String[listChapter.getItemCount()];
            
            
            String sql = "Select id,name,marks from questions where level in(?,?) and chapter_id in(";
            if(listChapter.getSelectedItem() != null){
                 chapNames[0] = listChapter.getSelectedItem();
            }
            else{
                chapNames = listChapter.getSelectedItems();
            }
            int[] ids = getChapterId(chapNames,flag);
            int totalMarks = 0;
            for(int i=0;i<ids.length;i++)
            {
                sql += ids[i]+","; 
            }
            sql = sql.substring(0, sql.length() - 1);
            if(cmbMarks.getSelectedItem().equals("25")){
                sql += ") and marks in(2,4)";
            }else {
                sql += ")";
            }
            ps = conn.prepareStatement(sql);
            if(cmbLevel.getSelectedItem().equals("Easy"))
            {
                ps.setString(1,"Easy");
                ps.setString(2,"Moderate");
            }
            else{
                ps.setString(1,"Moderate");
                ps.setString(2,"Difficult");
            }
            rs = ps.executeQuery();
            int marks = 0;
            int requiredMarks = Integer.parseInt(cmbMarks.getSelectedItem().toString());
            if(rs.next() == false){
                JOptionPane.showMessageDialog(this,"No questions are assigned to these chapters!");
            }
            else{
                do
                {
                    questions.put(rs.getString("name"),rs.getInt("marks"));  
                    totalMarks += rs.getInt("marks");
                }while(rs.next());
                if(additionalQues.isSelected()){
                    if(Integer.parseInt(cmbMarks.getSelectedItem().toString()) <= 30){
                        requiredMarks = Integer.parseInt(cmbMarks.getSelectedItem().toString())+10;
                        flag1= true;
                    }else if(Integer.parseInt(cmbMarks.getSelectedItem().toString()) <= 60){    
                        requiredMarks = Integer.parseInt(cmbMarks.getSelectedItem().toString())+20;
                        flag1 = true;
                    }else{
                        requiredMarks = Integer.parseInt(cmbMarks.getSelectedItem().toString())+40;
                        flag1 = true;
                    }
                }else{
                    flag1=false;
                }
                if(totalMarks >= requiredMarks){
                  
                    createFile(questions,flag1);
//                    clearfields();
                    JOptionPane.showMessageDialog(this,"Question Paper Generated Successfully!");
                }
                else{
                    JOptionPane.showMessageDialog(this,"Not enough questions in this chapter!");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void createFile(Map<String,Integer> questions,boolean myflag)
    {
            try{
                
                
                ArrayList<String> ques =new  ArrayList<>();
                ArrayList<Integer> ques_marks =new  ArrayList<>();
                File file = new File("Custom_Paper.txt");
                FileWriter writer = new FileWriter(file,false);
                writer.write("\t\t\t\tTest Paper \t\n\n");
                int marks2 = Integer.parseInt(cmbMarks.getSelectedItem().toString());
                int og_marks = marks2;
                String str="";
                int countQuestion = 1;
                    int marks = 0;
                    int qid = 1;
                     int marksPerQues=0;
                    Object[] crunchifyKeys = questions.keySet().toArray();
                    ArrayList<String> usedKeys = new ArrayList<>();
                    int totalQuestions =1;
                if(myflag){//additional dene hai 
                   writer.write("Choice Based Qustions:\t\t\tTotal Marks : "+marks2+"\n\n");
                    System.out.println("hi");
                    
                    if(Integer.parseInt(cmbMarks.getSelectedItem().toString()) <= 30){
                        marks2 += 10;
                        

                    }else if(Integer.parseInt(cmbMarks.getSelectedItem().toString()) <= 60){    
                        marks2 += 20;
                        
                    }else{
                        marks2 += 40;
                        

                    }   
                    marksPerQues = marks2;
                }
                else{
                    writer.write("Attempt All Questions:\t\t\tTotal Marks : "+marks2+"\n\n");
                    marksPerQues = marks2/totalQuestions;
                    
                }
                    
                   
                    if(checkSubQuestions.isSelected()){
                        totalQuestions += Integer.parseInt(cmbNoOfQuestions.getSelectedItem().toString())-1;
                        str+="\n\nQ"+qid+". Attempt all questions\n";
                        marksPerQues = marks2/totalQuestions;
                        if(myflag){
                            while(og_marks%marksPerQues!=0){
                            totalQuestions++;
                            marksPerQues = marks2/totalQuestions;
                                System.out.println(totalQuestions);
                        }                        
                    }

                        
                    }
                    
                    while(qid <= totalQuestions){
                        System.out.println(marksPerQues);
                        while(marks < marksPerQues){
                            Object key = crunchifyKeys[new Random().nextInt(crunchifyKeys.length)];
                            int checkMarks = marks + Integer.parseInt((questions.get(key)).toString());
                            
                            if(usedKeys.contains(key.toString()) || (checkMarks > marksPerQues)){
                                if(checkMarks > marksPerQues){
                                usedKeys = new ArrayList<>();
                                qid=1; 
                                str="\n\nQ"+qid+". Attempt all questions\n";
                                marks = 0;
                                countQuestion=1; 
                              }
                              continue;
                            }
                            else{
                            usedKeys.add(key.toString());
                            str += countQuestion+". "+key.toString()+ " \t\t\tMarks : "+Integer.parseInt((questions.get(key)).toString())+"\n";
                            marks+= Integer.parseInt((questions.get(key)).toString());
                            countQuestion++;
//                                System.out.println(str);
                            } 
                        }
                        qid++;
                        if(qid<= totalQuestions)
                             str+="\n\nQ"+qid+". Attempt all questions\n";
                        marks = 0;
                        countQuestion=1; 
                    } 
                    writer.write(str);
                   // writer.flush();
                    writer.close();
  
            } catch (IOException ex) {
                Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    
    public void clearfields()
    {
        cmbSubjects.setSelectedIndex(0);
        cmbMarks.setSelectedIndex(-1);
        cmbMarks.setEnabled(false);
        cmbNoOfQuestions.setSelectedIndex(-1);
        cmbNoOfQuestions.setEnabled(false);
        cmbNoOfQuestions.removeAllItems();
        cmbLevel.setSelectedIndex(-1);
        cmbLevel.setEnabled(false);
        checkSubQuestions.setSelected(false);
        checkSubQuestions.setEnabled(false);
        additionalQues.setEnabled(false);
        additionalQues.setSelected(false);
        listChapter.removeAll();
        listChapter.setEnabled(false);
    }
    
    public void noOfQuestions(){
        int marks = Integer.parseInt(cmbMarks.getSelectedItem().toString());
        if(marks==30){
            cmbNoOfQuestions.addItem("3");          
        }else if(marks==40){
            cmbNoOfQuestions.addItem("4");          
        }
        else if(marks==50){
            cmbNoOfQuestions.addItem("5");          
        }else if(marks==60){
            cmbNoOfQuestions.addItem("3");  
            cmbNoOfQuestions.addItem("6");  
        }else if(marks==80){
            cmbNoOfQuestions.addItem("4");  
            cmbNoOfQuestions.addItem("8");  
            cmbNoOfQuestions.addItem("10");  
        }else if(marks==100){
            cmbNoOfQuestions.addItem("5");  
            cmbNoOfQuestions.addItem("10");  
        }else{
            cmbNoOfQuestions.addItem("6");  
            cmbNoOfQuestions.addItem("12");  
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbMarks = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        listChapter = new java.awt.List();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbSubjects = new javax.swing.JComboBox<>();
        btnGenerate = new javax.swing.JButton();
        btnPreview1 = new javax.swing.JButton();
        cmbLevel = new javax.swing.JComboBox<>();
        additionalQues = new javax.swing.JCheckBox();
        cmbNoOfQuestions = new javax.swing.JComboBox<>();
        checkSubQuestions = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel1.setText("Select Chapters  : ");

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel2.setText("Enter Marks : ");

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel3.setText("Select Level");

        cmbMarks.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        cmbMarks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "30", "40", "50", "60", "80", "100", "120" }));
        cmbMarks.setEnabled(false);
        cmbMarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMarksActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/knowledge.png"))); // NOI18N

        listChapter.setEnabled(false);
        listChapter.setMultipleMode(true);
        listChapter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                listChapterItemStateChanged(evt);
            }
        });
        listChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listChapterActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel5.setText("No of Questions : ");

        jLabel6.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel6.setText("Choose a Subject");

        cmbSubjects.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectsActionPerformed(evt);
            }
        });

        btnGenerate.setBackground(new java.awt.Color(51, 51, 51));
        btnGenerate.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnGenerate.setForeground(new java.awt.Color(255, 255, 255));
        btnGenerate.setText("Generate Paper");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnPreview1.setBackground(new java.awt.Color(255, 255, 255));
        btnPreview1.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnPreview1.setForeground(new java.awt.Color(51, 51, 51));
        btnPreview1.setText("Preview Paper");
        btnPreview1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreview1ActionPerformed(evt);
            }
        });

        cmbLevel.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        cmbLevel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Moderate", "Difficult" }));
        cmbLevel.setEnabled(false);
        cmbLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbLevelActionPerformed(evt);
            }
        });

        additionalQues.setBackground(new java.awt.Color(255, 255, 255));
        additionalQues.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        additionalQues.setText("Add Optional Questions");
        additionalQues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                additionalQuesActionPerformed(evt);
            }
        });

        cmbNoOfQuestions.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        cmbNoOfQuestions.setEnabled(false);

        checkSubQuestions.setBackground(new java.awt.Color(255, 255, 255));
        checkSubQuestions.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        checkSubQuestions.setText("Paper with Sub-Questions");
        checkSubQuestions.setEnabled(false);
        checkSubQuestions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkSubQuestionsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(329, 329, 329)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel6)
                        .addComponent(jLabel1)
                        .addComponent(listChapter, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                        .addComponent(cmbSubjects, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnPreview1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(cmbMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(checkSubQuestions)
                            .addComponent(additionalQues)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(58, 58, 58)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cmbNoOfQuestions, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)))
                            .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(111, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel4)
                .addGap(59, 59, 59)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbSubjects, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkSubQuestions)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbNoOfQuestions, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(32, 32, 32)
                        .addComponent(additionalQues, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(listChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPreview1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
        String temp[] = listChapter.getSelectedItems();
        if(cmbSubjects.getSelectedItem() != null && cmbMarks.getSelectedItem() != null && (listChapter.getSelectedIndex() != -1 || temp.length>0) && cmbLevel.getSelectedItem() != null)
        {          
            generatePaper();
        }
        else{
            JOptionPane.showMessageDialog(this,"Please fill all details!");
        }
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void btnPreview1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreview1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPreview1ActionPerformed

    private void cmbLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbLevelActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_cmbLevelActionPerformed

    private void cmbSubjectsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectsActionPerformed
        // TODO add your handling code here:
        
        listChapter.setEnabled(true);
        if(flag2){
            getChapters();
        }
        
    }//GEN-LAST:event_cmbSubjectsActionPerformed

    private void listChapterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_listChapterItemStateChanged
        // TODO add your handling code here:
        cmbMarks.setEnabled(true);
    }//GEN-LAST:event_listChapterItemStateChanged

    private void cmbMarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMarksActionPerformed
        // TODO add your handling code here:
        if(flag2){
            checkSubQuestions.setEnabled(true);
            checkSubQuestions.setSelected(false);
            cmbNoOfQuestions.removeAllItems();
            
            noOfQuestions();
            cmbNoOfQuestions.setEnabled(false);
            
            cmbLevel.setEnabled(true);
        }
    }//GEN-LAST:event_cmbMarksActionPerformed

    private void listChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listChapterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_listChapterActionPerformed

    private void checkSubQuestionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkSubQuestionsActionPerformed
        // TODO add your handling code here:
        if(checkSubQuestions.isSelected()) {
            cmbNoOfQuestions.setEnabled(true);
            if(cmbNoOfQuestions.getItemCount() <= 0)
             noOfQuestions();
        }
        else{
            cmbNoOfQuestions.setEnabled(false);
            cmbNoOfQuestions.removeAllItems();
        }
    }//GEN-LAST:event_checkSubQuestionsActionPerformed

    private void additionalQuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_additionalQuesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_additionalQuesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomizePaper.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomizePaper.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomizePaper.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomizePaper.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomizePaper(teacher_id).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox additionalQues;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnPreview1;
    private javax.swing.JCheckBox checkSubQuestions;
    private javax.swing.JComboBox<String> cmbLevel;
    private javax.swing.JComboBox<String> cmbMarks;
    private javax.swing.JComboBox<String> cmbNoOfQuestions;
    private javax.swing.JComboBox<String> cmbSubjects;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private java.awt.List listChapter;
    // End of variables declaration//GEN-END:variables
    private static int teacher_id;
    private boolean flag=false;
    private boolean flag1=false;
    private boolean flag2=false;
    private boolean flag3 = false;
}


