
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Isha
 */

public class UnitTest extends javax.swing.JFrame {

    /**
     * Creates new form UnitTest
     */
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection conn = null;
    TeacherPanel teacherPanelObj;
    public UnitTest(int teacher_id) {
        initComponents();
        setResizable(false);
        
        conn = MySQLConnect.connectionDB();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2 - getWidth()/2 , dimension.height/2 - getHeight()/2);
        this.teacher_id = teacher_id;
        
        getSubjects(teacher_id);
        cmbSubjects.setSelectedIndex(-1);
        cmbLevel.setSelectedIndex(-1);
        flag = true;
         
    }
    public void getSubjects(int teacher_id){
         
        try {
            if(cmbSubjects.getItemCount() > 0){
                    for(int i=cmbSubjects.getItemCount()-1;i>=0;i--){
                        cmbSubjects.removeItemAt(i);
                    }
            }
             String sql = "SELECT  name from subjects where id in (select subject_id from subject_teacher where teacher_id = ?)";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,teacher_id);
             rs = ps.executeQuery();
             
             if(rs.next() == false){
                    if(cmbSubjects.getItemCount()-1 >= 0)
                    cmbSubjects.removeItemAt(0);
                }
                else{
                    do{       
                        cmbSubjects.addItem(rs.getString("name"));
                    }while(rs.next());
                }
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }         
    }
    
    public void getChapters(){
         try {
             int subject_id = getSubjectID(cmbSubjects.getSelectedItem().toString());

            listChapter.removeAll();
             
             String sql = "SELECT  name from chapters where subject_id = ?";
             ps = conn.prepareStatement(sql);
             ps.setInt(1,subject_id);
             rs = ps.executeQuery();
             if(rs.next() == false){
      
                }
                else{
                    
                    do{    
                        
                        listChapter.add(rs.getString("name"));
                    }while(rs.next());
                    //listChapter.select(-1);
                }
             
             //listChapter.setSelectedIndex(-1);
                
         } catch (SQLException ex) {
             Logger.getLogger(TeacherPanel.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    private int getSubjectID(String name){
        try{
        String sql = "SELECT id from subjects where name= ? ";
            ps = conn.prepareStatement(sql);
            ps.setString(1,name);
            rs = ps.executeQuery();
            if(rs.next()){
//                System.out.println(rs.getInt("id"));
                return rs.getInt("id");
            }
            return -1;
        }
        catch(SQLException e){
                 JOptionPane.showMessageDialog(null,"Some Issue : " + e);
                 return -1;
        }
    }
    
    public int[] getChapterId(String names[],boolean flag){
        
        int count = names.length;
        int[] ids = new int[count];
         try {
             String sql = "Select id from chapters where name in(";
             for(int i=0;i<count;i++)
             {    
                 sql += "'";
                 sql += names[i]+"',";
             }
             sql = sql.substring(0, sql.length() - 1);
             sql += ")";
             ps = conn.prepareStatement(sql);
             
             rs = ps.executeQuery();
             int i=0;
             while(rs.next()){
                 ids[i] = rs.getInt("id");
                 i++;
             }
             return ids;
         } catch (SQLException ex) {
            
              JOptionPane.showMessageDialog(this,"Some Issue : " + ex);
               return ids;
         }
     }
    private void generateUnitPaper(){
      
        try {
            //Select name,marks from questions where (level in("Easy","Moderate")) and chapter_id in(38,42) and marks in(2,4)
            Map<String,Integer> questions = new HashMap<>();
            Map<String,Integer> finalQuestions = new HashMap<>();
            
            String[] chapNames = new String[listChapter.getItemCount()];
            
            String sql = "Select id,name,marks from questions where level in(?,?) and chapter_id in(";
            if(listChapter.getSelectedItem() != null){
                 chapNames[0] = listChapter.getSelectedItem();
            }
            else{
                chapNames = listChapter.getSelectedItems();
            }
            int[] ids = getChapterId(chapNames,flag);
            int totalMarks = 0;
            for(int i=0;i<ids.length;i++)
            {
                sql += ids[i]+","; 
            }
            sql = sql.substring(0, sql.length() - 1);
            sql += ") and marks in(2,4)";    
            ps = conn.prepareStatement(sql);
            if(cmbLevel.getSelectedItem().equals("Easy"))
            {
                ps.setString(1,"Easy");
                ps.setString(2,"Moderate");
            }
            else{
                ps.setString(1,"Moderate");
                ps.setString(2,"Difficult");
            }
            rs = ps.executeQuery();
            int marks = 0;
            int requiredMarks = 20;
            while(rs.next())
            {
               questions.put(rs.getString("name"),rs.getInt("marks"));  
               totalMarks += rs.getInt("marks");
            }
            if(additionalQues.isSelected())
            {    requiredMarks = 26;
                 flag1 = true;
            }
            if(totalMarks >= requiredMarks){
            Object[] crunchifyKeys = questions.keySet().toArray();
            ArrayList<String> usedKeys = new ArrayList<>();
            
                while(marks < requiredMarks){

                    Object key = crunchifyKeys[new Random().nextInt(crunchifyKeys.length)];
                    if(usedKeys.contains(key.toString())){
                        continue;
                    }
                    usedKeys.add(key.toString());
                    finalQuestions.put(key.toString(),Integer.parseInt((questions.get(key)).toString()));
                    marks+= Integer.parseInt((questions.get(key)).toString());
                }
                createFile(finalQuestions,flag1);
                clearfields();
                JOptionPane.showMessageDialog(this,"Question Paper Generated Successfully!");
            }
            else{
                JOptionPane.showMessageDialog(this,"Not enough questions in this chapter!");
            }
            //createFile(Questions,Marks);
        } catch (SQLException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void createFile(Map<String,Integer> questions,boolean flag)
    {
        try {
            FileWriter writer = new FileWriter("UnitTest.txt",false);
            writer.write("\t\t\t\tUnit Test Paper \t\n\n");
            if(flag)
                writer.write("Choice Based Qustions:\t\t\tTotal Marks : 20\n\n");
            else
                writer.write("Attempt All Questions:\t\t\tTotal Marks : 20\n\n");
            int count = 1;
            int size = questions.size();
            
            for (Map.Entry<String,Integer> entry : questions.entrySet()) {
		String str = "Q"+count+". "+entry.getKey() + "\t\t\t Marks: " + entry.getValue()+"\n";
                count++;
                writer.write(str);
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void clearfields()
    {
        cmbSubjects.setSelectedIndex(-1);
        cmbLevel.setSelectedIndex(-1);
        cmbLevel.setEnabled(false);
        listChapter.removeAll();
        listChapter.setEnabled(false);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtMarks = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbLevel = new javax.swing.JComboBox<>();
        btnPreview = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cmbSubjects = new javax.swing.JComboBox<>();
        additionalQues = new javax.swing.JCheckBox();
        btnGenerate = new javax.swing.JButton();
        listChapter = new java.awt.List();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/knowledge.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel2.setText("Select Chapters :");

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel3.setText("Marks :");

        txtMarks.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        txtMarks.setText("20");
        txtMarks.setEnabled(false);

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel4.setText("Select Level :");

        cmbLevel.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        cmbLevel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Moderate", "Difficult" }));
        cmbLevel.setEnabled(false);

        btnPreview.setBackground(new java.awt.Color(255, 255, 255));
        btnPreview.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnPreview.setForeground(new java.awt.Color(51, 51, 51));
        btnPreview.setText("Preview Paper");
        btnPreview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviewActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel5.setText("Select Subject :");

        cmbSubjects.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        cmbSubjects.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectsActionPerformed(evt);
            }
        });

        additionalQues.setBackground(new java.awt.Color(255, 255, 255));
        additionalQues.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        additionalQues.setText("Add Optional Questions");
        additionalQues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                additionalQuesActionPerformed(evt);
            }
        });

        btnGenerate.setBackground(new java.awt.Color(51, 51, 51));
        btnGenerate.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnGenerate.setForeground(new java.awt.Color(255, 255, 255));
        btnGenerate.setText("Generate Paper");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        listChapter.setEnabled(false);
        listChapter.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        listChapter.setMultipleMode(true);
        listChapter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                listChapterItemStateChanged(evt);
            }
        });
        listChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listChapterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5)
                            .addComponent(cmbSubjects, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(cmbLevel, 0, 118, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1)
                                    .addComponent(listChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(additionalQues)
                                .addGap(64, 64, 64))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(btnPreview)
                        .addGap(27, 27, 27)
                        .addComponent(btnGenerate)))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cmbSubjects, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(listChapter, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(additionalQues))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtMarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPreview, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void additionalQuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_additionalQuesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_additionalQuesActionPerformed

    private void btnPreviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviewActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnPreviewActionPerformed

    private void cmbSubjectsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectsActionPerformed
        listChapter.setEnabled(true);
        if(flag)
            getChapters();
    }//GEN-LAST:event_cmbSubjectsActionPerformed

    private void listChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listChapterActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_listChapterActionPerformed

    private void listChapterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_listChapterItemStateChanged
        // TODO add your handling code here:
        cmbLevel.setEnabled(true);
    }//GEN-LAST:event_listChapterItemStateChanged

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
        if(cmbLevel.getSelectedItem() != null && cmbSubjects.getSelectedItem() != null && (listChapter.getSelectedItem() != null || listChapter.getSelectedItems() != null))
        {
            generateUnitPaper();
        }
        else{
            JOptionPane.showMessageDialog(this,"Please fill all details!");
        }
    }//GEN-LAST:event_btnGenerateActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UnitTest(teacher_id).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox additionalQues;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnPreview;
    private javax.swing.JComboBox<String> cmbLevel;
    private javax.swing.JComboBox<String> cmbSubjects;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private java.awt.List listChapter;
    private javax.swing.JTextField txtMarks;
    // End of variables declaration//GEN-END:variables
    private static int teacher_id;
    private boolean flag = false;
    private boolean flag1 = false;
}
